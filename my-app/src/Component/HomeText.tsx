import React from 'react'

export default function homeText() {
  return (
    <>


  <div className="flex flex-col justify-center items-center pt-28">
    <div className="md:w-1/3 sm:w-full rounded-lg shadow-lg bg-white my-3">       
      	<div className="px-10 py-5 text-gray-600">
        (en attente de la version définitive) "Nozama en France : une
            contribution positive à l’économie Nous servons avec passion nos clients
            en France. Toutefois, si nous pensons d’abord à eux, nous accordons
            également la plus grande importance à notre empreinte économique,
            sociale et environnementale au niveau local. En mettant toute notre
            énergie, notre savoir-faire et notre capacité d’innovation au service
            des Français, nous contribuons à la croissance de l’économie française.
            Nous souhaitions partager un aperçu de la manière dont nous contribuons à
            la croissance de l’économie française, à la création de milliers
            d’emplois, au financement des services publics et du modèle social
            français ; tout en prenant soin de préserver l’environnement."
      	</div>
      
        </div>
	</div>

    </>
  )
}
