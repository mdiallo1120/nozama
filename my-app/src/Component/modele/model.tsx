export type Offer = {
    id: number;
    offername:string;
    offerrole: string;
    offercontent: string;
    date: string;
    location: string;
    contracttype: string;
}
