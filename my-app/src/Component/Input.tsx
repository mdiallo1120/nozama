import { Component } from 'react';

type State = {}
type Props = {

    name?: string;
    label?: string;
    disable?: boolean;
    className?: string;
    ref?: string;
    placeholder?: string;
    type?: string;
    id?:string;
    pattern?: string;
    value?: string;
    onChange?: (e: any) => void; 
    required?: boolean;
    maxLength?:number ;
    minLength?:number ;
    
}

 
export default class Input extends Component<Props, State> {
render() {

  const { name, className, disable,ref,placeholder,type, pattern, value , onChange, required, maxLength, minLength, id } = this.props
  
    return (
       
            <input className={ disable ? `disable ${className}` : className} name={name} ref={ref} placeholder={placeholder} type={type} pattern={pattern} value={value} 
            onChange={onChange} required={required} maxLength={maxLength} minLength={minLength} id={id} />
        
      )
    }
  }
