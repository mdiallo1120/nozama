<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Tags;
use ApiPlatform\Core\Annotation\ApiResource;


#[ORM\Entity(repositoryClass: ProductRepository::class)]


#[ ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    attributes: ["pagination_client_items_per_page" => true]
    )]


class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["read"])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read", "write"])]
    private $title;

    #[ORM\Column(type: 'string', length: 500)]
    #[Groups(["read", "write"])]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read", "write"])]
    private $image_link;

    #[ORM\Column(type: 'string')]
    #[Groups(["read", "write"])]
    private $price;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read", "write"])]
    private $stock;

    #[ORM\ManyToMany(targetEntity: Clients::class, mappedBy: 'product')]
    private $clients;

    #[ORM\ManyToMany(targetEntity: tags::class, inversedBy: 'products')]
    private $tags;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImageLink(): ?string
    {
        return $this->image_link;
    }

    public function setImageLink(string $image_link): self
    {
        $this->image_link = $image_link;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?string
    {
        return $this->stock;
    }

    public function setStock(string $stock): self
    {
        $this->stock = $stock;

        return $this;
    }


    /**
     * @return Collection<int, Clients>
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Clients $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->addProduct($this);
        }

        return $this;
    }

    public function removeClient(Clients $client): self
    {
        if ($this->clients->removeElement($client)) {
            $client->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, tags>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }
    public function setTags(Tags $tags): self
    {
        $this->Tags = $Tags;
    }

    public function addTag(tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(tags $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }
}
