import React, { useEffect } from 'react';
import Button from './Button';
import axios from 'axios';


export default function Card() {


    
    // async function fetchText() {
        // let response = await fetch('https://localhost:8000/api/products/');
    
        // console.log(response.status); // 200
        // console.log(response.statusText); // OK
    
        // if (response.status === 200) {
        //     let data =  response.text();
            
        //     // handle data
        // }
    // }
    const api = async () =>{
//    await axios.get('https://localhost:8000/api/products/')
   
    let baseUrl = 'https://localhost:8000/api/Products/1' 

    const response = await axios.get(baseUrl, {
      validateStatus: (status) => {
        return status < 500; // Resolve only if the status code is less than 500
      }
    });
  
    if (response.request.status == 200){
      let data = await response.data
      if(data.hasOwnProperty(['hydra:member'])){
        return ( {products: data['hydra:member'], pages: data['hydra:view']})
      }
        return (data)
    }
      return ('no')
    
  }
  
  useEffect(() => {
      api();
  })


  return (
        <>
        {/* <div className="w-full md:w-1/2 xl:w-1/3 px-4">
            <div className="bg-white rounded-lg overflow-hidden mb-10">
               <img
                  src="https://cdn.tailgrids.com/1.0/assets/images/cards/card-01/image-03.jpg"
                  alt="image" className="w-full" />
               <div className="p-8 sm:p-9 md:p-7 xl:p-9 text-center">
                  <h3>
                    <a
                    href="javascript:void(0)" className="font-semibold text-dark text-xl  sm:text-[22px] md:text-xl lg:text-[22px] xl:text-xl 2xl:text-[22px] mb-4 block   hover:text-primary">
                    Creative Card Component designs graphic elements
                    </a>
                  </h3>
                  <p className="text-base text-body-color leading-relaxed mb-7">
                     Lorem ipsum dolor sit amet pretium consectetur adipiscing
                     elit. Lorem consectetur adipiscing elit.
                  </p>
                  <a
                     href="javascript:void(0)" className=" inline-block py-2 px-7 border border-[#E5E7EB] rounded-full text-base text-body-color font-medium hover:border-primary  hover:bg-primary hover:text-white transition "> 
                  View Details
                  </a>
               </div>
               <Button text="Acheter"/>
               <Button text="Ajouter au Panier"/>


            </div>
         </div> */}
      
   
        </>
  )
}
