import { useState } from 'react';
import { Link } from 'react-router-dom';
// import Nozama from './images/Nozama.png'

const Navbar = () => {
  let [ showNav, setShowNav] = useState(false); 

  
    return (
      <>
 
        <div className="bg-primary lg:bg-white sticky z-50 px-20">
            <div className="flex items-center justify-between py-4 z-50">
              <div>
                <Link to="/"> <img className="h-10 w-10 self-center" src="https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/70c29c6e-e708-409a-a54f-3162c528b92f.png"/>
                <a href="/home" className="text-2xl no-underline text-grey-darkest hover:text-blue-dark font-Bangers font-bold">Nozama</a>
                </Link>
              </div>
              <div className="hidden sm:flex sm:items-center ">

                  <Link to="/" className="px-4">Home </Link>
                  <Link to="/S'identifier" className="px-4">S'identifier</Link>
                  <Link to="/S'enregistrer" className="px-4">S'enregistrer</Link>
              </div>
                <h3 className="sm:hidden md:hidden lg:hidden font-semibold  text-center text-xl uppercase ">we are hiring</h3>
              {showNav ? (
                <button className="sm:hidden cursor-pointer " onClick={() => setShowNav(!showNav)}>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                      <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                  </svg>
                </button>
              ) : ( 
                <button className="sm:hidden cursor-pointer " onClick={() => setShowNav(!showNav)}>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-7 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                      <path strokeLinecap="round" strokeLinejoin="round" d="M4 6h16M4 12h8m-8 6h16" />
                  </svg>
                </button>
              )}
            </div>
            <div className={(showNav ? "absolute" : "hidden") + " bg-primary z-5 w-screen"}>
              <div className="flex flex-col sm:hidden bg-white">
               <Link to="/" className="px-6 py-3 text-xl" onClick={() => setShowNav(!showNav)}>Home</Link>
                <Link to="/S'identifier" className="px-6 py-3 text-xl" onClick={() => setShowNav(!showNav)}>S'identifier</Link>
                <Link to="/S'enregistrer" className="px-6 py-3 text-xl" onClick={() => setShowNav(!showNav)}>S'enregistrer</Link>
              </div>
            </div>
          </div>


 

      </>
    )
}

export default Navbar;
