import React, { Component } from 'react'
type State = {}
type Props = {
    text?: string,
    className?: string,
    onClick?(e: React.MouseEvent<any>): any;
}

export default class Button extends Component<Props, State> {
    render() {
        const { text, className, onClick, children = 'Click' }:any = this.props
        const value = text || children
        const property=" text-bg-color bg-primary rounded-md border border-primary p-3 transition "
        return (
            <button className={`${property} ${className}`} onClick={onClick}>{value}</button>
        )
    }
}
