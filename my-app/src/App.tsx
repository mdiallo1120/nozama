// import React from 'react';
// import logo from './logo.svg';
import { BrowserRouter as Router,Routes, Route } from "react-router-dom";
import Navbar from "./Component/Navbar";
import Homepage from"./Component/Homepage/Homepage";
import SignIn from './Component/SignIn';
import SignUp from './Component/SignUp';
import './App.css';

function App() {
  return (
    <div className="font-sans flex flex-col min-h-screen w-full">
    <div className="flex-grow">
    <Router>
      <Navbar />
      <Routes>
         <Route path="/" element = {<Homepage />} />
         <Route path="/S'identifier" element = {<SignIn />} />
         <Route path="/S'enregistrer" element={<SignUp />} />
      {/*  
        <Route path="offer/:offerId" element={<IdvOffer />} />
        <Route path="/Apply/:offerId/:offerName" element={<Apply />} />
        <Route path="/thank-you" element = {<ConfirmationPage />} />
        <Route path="/thank-you-apply/:offerName" element = {<ConfirmationPageApply />} />
        <Route path="*" element = {<ErrorPage />} /> */}
      </Routes>
    </Router>
    </div>
  {/* <Footer /> */}
</div>
  )
}

export default App;
